# Codebeispiele für die Bachelorarbeit

## Eine nachvollziehbare Implementation von polynombasierten Signatur- und Verschüsselungsverfahren

### Voraussetzungen

Für die Benutzung der zur Verfügung gestellten Klassen und Skripte werden folgende Dinge vorausgesetzt:

1. Eine `python2.7`-Umgebung (getestet unter Version 2.7.10)
1. Eine `sagemath`-Umgebung (getestet unter Version 7.5.1)

_Die Quelltexte wurden unter macOS 10.12.6 erstellt und getestet_

### Anwendung der Codebeispiele

1. Herunterladen und entpacken der hier zur Verfügung gestellten Quelldateien
2. Mit dem CLI zu den Quellen navigieren und `sage` ausführen
3. Entweder `bsp_*.py` kopieren und einfügen oder mit `load('bsp_*.py')` ausführen
