#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sage.all import *

from classes.MatsumotoImaiAExample import MatsumotoImaiAExample

# Endlicher Körper mit 5 Elementen:
finite_field = GF(2**2, 'a')
(a,) = finite_field.gens()

n = 3  # Drei Variablen

"""TEST MIA"""

# MIA mit den Schlüsseln initialisieren
# MatsumotoImaiAExample setzt die sonst zufälligen Werte
# wie in den Beispielen und printed mehr
MIA = MatsumotoImaiAExample(finite_field, n)


# Schlüssel im sage CLI nutzbar machen
private_key = MIA.private_key
public_key = MIA.public_key

# Schlüssel ausgeben
print("Privater Schlüssel:")
print("S:\n{}x + {}".format(private_key.S.M, private_key.S.y))
print("Pr:\n{}".format(private_key.Pr))
print("S:\n{}x + {}".format(private_key.T.M, private_key.T.y))

print("\nÖffentlicher Schlüssel:")
print("P:\n{}".format(public_key.P))

# Die Nachricht soll (1, a, a^2) sein
msg = vector(finite_field, [1, a, a**2])
print("\n\nmsg =", msg)

# Nachricht verschlüsseln
enc = public_key.encrypt(msg)
print("public_key.encrypt(msg): enc =", enc)

# Verifizieren, dass enc auch entschlüsselt wird
print("MIA.decryt(enc):", MIA.decrypt(enc))

# Signatur erstellen
msg = vector(finite_field, [a, 1, 1])
print("\nmsg =", msg)
sig = MIA.sign(msg)
print("sig =", sig)

# Signatur verifizieren
print("public_key.verify(msg, sig):", public_key.verify(msg, sig))
