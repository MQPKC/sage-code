#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function


class PolynomialSystem(object):
    def __init__(self, P):
        self.P = P

    def __repr__(self):
        return self.P

    def __call__(self, variables):

        ret = []
        for p in self.P:
            ret.append(p(variables))

        return ret
