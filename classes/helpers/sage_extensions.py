#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from sage.all import random_matrix, random_vector, random


def random_invertible_matrix(finite_field, n):
    matrix = random_matrix(finite_field, n)

    while not matrix.is_invertible():
        matrix = random_matrix(finite_field, n)

    return matrix


def random_value(finite_field):
    return random_vector(finite_field, 1)[0]


def random_between(j, k):
    a = int(random() * (k - j + 1)) + j
    return a
