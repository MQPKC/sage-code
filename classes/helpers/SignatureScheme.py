#!/usr/bin/sage
# -*- coding: utf-8 -*-

from sage.all import *


class SignatureScheme(object):
    """
    SignatureScheme ist ein interface. Entprechend muss es zwingend
    mit private_key und public_key initialisiert werden
    """

    def sign(self, msg):
        '''generiere Signatur, die zu wieder zu msg `verschlüsselt`'''

        # Sollte `msg` eine Liste sein -> Typecast
        if not isinstance(msg, sage.structure.element.Vector):
            msg = vector(msg)

        _t = self.private_key.T.inverse(msg)
        _p = self.invert_MQ(_t)
        return self.private_key.S.inverse(_p)

    def invert_MQ(self, msg):
        '''Platzhalter für alle Signatur- und Verschlüsselungsverfahren'''
        raise NotImplementedError

    def verify(self, msg, sig):
        # da SignatureScheme nicht direkt von PublicKey erbt wird
        # die Verifikation extra zur Verfügung gestellt
        return self.public_key.verify(msg, sig)
