#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sage.all import *


class AffineTransformation():
    def __init__(self, M, y):
        """
        Eine affine Transformation wird mit einer invertierbaren
        Matrix und einem beliebigen Vektor initialisiert
        """

        # Abbruchbedingung für nicht invertierbare Matrizen
        if not M.is_invertible():
            exit(1)

        self.M = M  # invertierbare Matrix
        self.y = y  # Vektor

    def __call__(self, v):
        """Anwenden der Transformation auf den gegebenen Vektor"""

        # Sollte `v` eine Liste sein -> Typecast
        if not isinstance(vector, sage.structure.element.Vector):
            v = vector(v)

        return (self.M * v) + self.y

    def inverse(self, v):
        """Anwenden der inversen Transformation auf den gegebenen Vektor"""

        # Sollte `v` eine Liste sein -> Typecast
        if not isinstance(vector, sage.structure.element.Vector):
            v = vector(v)

        return self.M.inverse() * (v - self.y)
