#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sage.all import vector
import cPickle as pickle

from .PublicKey import PublicKey


class PrivateKey(object):
    def __init__(self, S, Pr, T, path=None):
        # wird ein Schlüssel als Dateipfad übergeben, so wird dieser geladen
        if path is not None:
            (S, Pr, T) = self.load(path)

        self.S = S    # Affine Transformation
        self.Pr = Pr  # Polynomsystem
        self.T = T    # Affine Transformation

    def generate_public_key(self):
        # in `x` sind die Variablen x1,...,xn
        x = vector(self.Pr[0].parent().gens())

        # angewendet auf die Verschlüsselung ergibt
        # dies das öffentliche Polynom
        spt = self.encrypt(x)

        return PublicKey(spt)

    def encrypt(self, message):
        # Die Verschlüsselung wurde angepasst um nicht von
        # dem PublicKey abzuhängen sondern von (S,Pr,T)
        sx = list(self.S(vector(message)))
        spx = []
        for p in self.Pr:
            spx.append(p(sx))
        return self.T(vector(spx))

    """ Es folgt eine sehr einfache Möglichkeit den Schlüssel zu
    speichern und entsprechend auch wieder zu laden """

    def save(self, path="./keys/key.priv"):
        with open(path, 'wb') as output:
            pickle.dump((self.S, self.Pr, self.T),
                        output, pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        with open(path, 'rb') as output:
            return pickle.load(output)
