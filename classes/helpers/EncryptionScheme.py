#!/usr/bin/sage
# -*- coding: utf-8 -*-
from sage.all import *

from .SignatureScheme import SignatureScheme


class EncryptionScheme(SignatureScheme):
    """Da EncryptionScheme von SignatureScheme erbt, stehen alle Funktionen zur Verfügung.
    Wichtig ist auch hier, dass private_key und public_key gesetzt sind."""

    def encrypt(self, message):
        # da EncryptionScheme nicht direkt von PublicKey erbt wird
        # die Verschlüsseling extra zur Verfügung gestellt
        return self.public_key.encrypt(message)

    def decrypt(self, ciphertext):
        # verhält sich analog zu SignatureScheme.sign(msg)

        # Sollte `ciphertext` eine Liste sein -> Typecast
        if not isinstance(ciphertext, sage.structure.element.Vector):
            ciphertext = vector(ciphertext)

        _t = self.private_key.T.inverse(ciphertext)
        _p = self.invert_MQ(_t)
        return self.private_key.S.inverse(_p)
