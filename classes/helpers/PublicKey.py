#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
from sage.all import vector
import cPickle as pickle


class PublicKey(object):
    def __init__(self, public_polynomial=None, path=None):
        # wird ein Schlüssel als Dateipfad übergeben, so wird dieser geladen
        if public_polynomial is None and path is not None:
            self.P = self.load(path)
        else:
            # speichere das Polynomsystem:
            self.P = public_polynomial

    def __call__(self, vector):
        return self.encrypt(vector)

    def __repr__(self):
        return "PublicKey: {}".format(list(self.P))

    def encrypt(self, msg):
        # der Vektor `msg` muss als Liste an Sage übergeben werden
        # Sage kann dann eigenständig das Polynomsystem ausrechen
        msg = list(msg)
        solution = []
        for p in self.P:
            solution.append(p(msg))

        return vector(solution)

    def verify(self, msg, sig):
        # Vergleiche gegebene Singatur mit generierter Signatur
        return msg == self.encrypt(sig)

    """ Es folgt eine sehr einfache Möglichkeit den Schlüssel zu
    speichern und entsprechend auch wieder zu laden """

    def save(self, path="./keys/key.pub"):
        with open(path, 'wb') as output:
            pickle.dump(self.P, output, pickle.HIGHEST_PROTOCOL)

    def load(self, path):
        with open(path, 'rb') as output:
            return pickle.load(output, pickle.HIGHEST_PROTOCOL)
