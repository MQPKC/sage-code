#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function
from sage.all import *
from sys import exit

from .helpers.sage_extensions import random_value, random_invertible_matrix

from .helpers.AffineTransformation import AffineTransformation
from .helpers.PrivateKey import PrivateKey
from .helpers.PrivateKey import PublicKey
from .helpers.EncryptionScheme import EncryptionScheme


class MatsumotoImaiA(EncryptionScheme):
    """Matsumoto Imai Scheme A Implementation"""

    def phi(self, polynomail_a):
        return polynomail_a.list()

    def phi_inv(self, vector_a):
        return self.extension_field(vector_a)

    def __init__(self, finite_field, n, keyfile=None):
        self.finite_field = finite_field
        self.n = n

        # generate or load private & public key
        if keyfile is not None:
            self.private_key = PrivateKey(None, None, None, keyfile)
            self.public_key = self.private_key.generate_public_key()
        else:
            self.generate_keys(n)

    def generate_keys(self, n):
        k = self.finite_field
        (a,) = k.gens()

        ring = PolynomialRing(k, 'x')
        (x,) = ring.gens()
        gx = ring.irreducible_element(n)

        q = len(k)

        # alle Kandidaten für theta werden gesucht
        thetas = []
        qn = q**n - 1
        for theta in xrange(1, n):
            qd = q**theta + 1
            (common_divider, t, _) = xgcd(qd, qn)
            if common_divider == 1 and t > 0:
                thetas.append((theta, t))

        # ein zufälliges Theta wird ausgewählt
        if len(thetas) < 1:
            exit('Kein theta gefunden')

        (self.theta, self.theta_invers) = thetas[int(random() * len(thetas))]

        S = AffineTransformation(
            random_invertible_matrix(k, n), random_vector(k, n))
        T = AffineTransformation(
            random_invertible_matrix(k, n), random_vector(k, n))

        # der Erweiterungskörper wird gebaut
        multivariate_ring = PolynomialRing(k, 'x', n)
        extension_field = PolynomialRing(
            multivariate_ring, 't').quotient_ring(gx, 'T')
        self.extension_field = extension_field
        multi_vars = multivariate_ring.gens()

        Sx = list(S(vector(multi_vars)))

        pre_F = self.phi_inv(Sx)

        # F(X)
        post_F = pre_F * pre_F**(4**theta)

        Pr = S.inverse(vector(post_F.list()))
        SPTx = T(vector(post_F.list()))

        self.private_key = PrivateKey(S, Pr, T)
        self.public_key = PublicKey(SPTx)

    def invert_MQ(self, msg):
        X = self.phi_inv(list(msg))
        return vector(self.phi(X**self.theta_invers))
