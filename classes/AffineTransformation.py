from __future__ import absolute_import, division, print_function
from sage.all import *


class AffineTransformation():
    def __init__(self, M, y):
        """
        Eine affine Transformation wird mit einer invertierbaren
        Matrix und einem beliebigen Vektor initialisiert
        """

        # Abbruchbedingung für nicht invertierbare Matrizen
        if not M.is_invertible():
            exit(1)

        self.M = M  # invertierbare Matrix
        self.y = y  # Vektor

    def __call__(self, vector):
        """Anwenden der Transformation auf den gegebenen Vektor"""

        # Sollte `vector` eine Liste sein -> Typecast
        if not isinstance(vector, sage.structure.element.Vector):
            vector = vector(vector)

        return (self.M * vector) + self.y

    def inverse(self, vector):
        """Anwenden der inversen Transformation auf den gegebenen Vektor"""

        # Sollte `vector` eine Liste sein -> Typecast
        if not isinstance(vector, sage.structure.element.Vector):
            vector = vector(vector)

        return self.M.inverse() * (vector - self.y)
