#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sage.all import *

from classes.MatsumotoImaiA import MatsumotoImaiA

n = 5 # bitte prim wählen
finite_field = GF(2**8, 'a')

"""TEST MIA"""

# Initialize simple MIA
MIA = MatsumotoImaiA(finite_field, n)
public_key = MIA.public_key

# use a random vector
msg = random_vector(finite_field, n)
print("msg =", msg)

# Nachricht verschlüsseln
enc = public_key.encrypt(msg)
print("public_key.encrypt(msg): enc =", enc)

# Verifizieren, dass enc auch entschlüsselt wird
print("MIA.decryt(enc):", MIA.decrypt(enc))
