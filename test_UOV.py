#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sys import exit
from sage.all import *

from classes.UnbalancedOilAndVinegar import UnbalancedOilAndVinegar

# Variables that may be set by CLI
q = 5  # Number of elements in F
m = 4  # length of message
n = 6  # length of signature

# Computed Variables
if n <= m:
    exit(1)

oil = n - m  # number of oil variables
vinegar = m  # number of vinegar variables

# Finite field with q elements
finite_field = GF(q, 'a')

# Multivariate Polynomial Ring over
# `finite_field` with `n` variables
ring = PolynomialRing(finite_field, 'x', n)

"""TEST UOV"""

# Initialize simple UOV
UOV = UnbalancedOilAndVinegar(finite_field, n, m)

# use a random vector
msg = random_vector(finite_field, m)

# generate Signature
sig = UOV.sign(msg)

# verify signature is valid
print("msg =", msg)
print("sig =", sig)
print("UOV.verify(msg, sig):", UOV.verify(msg, sig))
