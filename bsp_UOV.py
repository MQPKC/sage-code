#!/usr/bin/sage
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
from sage.all import *

from classes.UnbalancedOilAndVinegarExample import *

# Endlicher Körper mit 5 Elementen:
finite_field = GF(5, 'a')

m = 2  # Zwei Gleichungen
n = 3  # Drei Variablen

"""TEST UOV"""

# UOV mit den Schlüsseln initialisieren
# UnbalancedOilAndVinegarExample setzt die sonst zufälligen Werte
# wie in den Beispielen und printed mehr
UOV = UnbalancedOilAndVinegarExample(
    finite_field, n, m, keyfile="./keys/bsp_uov.priv")

# Schlüssel im sage CLI nutzbar machen
private_key = UOV.private_key
public_key = UOV.public_key

# Schlüssel ausgeben
print("Privater Schlüssel:")
print("S:\n{}x + {}".format(private_key.S.M, private_key.S.y))
print("Pr:\n{}".format(private_key.Pr))
print("S:\n{}x + {}".format(private_key.T.M, private_key.T.y))

print("\nÖffentlicher Schlüssel:")
print("P:\n{}".format(public_key.P))

# Die Nachricht soll (1, 4) sein
msg = vector(finite_field, [1, 4])
print("\n\nmsg =", msg)

# Signatur erstellen
sig = UOV.sign(msg)
print("sig =", sig)

# Signatur verifizieren
print("public_key.verify(msg, sig):", public_key.verify(msg, sig))
